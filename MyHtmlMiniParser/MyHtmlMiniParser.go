package myhtmlminiparser

import (
	"bytes"
	"strings"

	"golang.org/x/net/html"
)

type info struct {
	Href string `json:"MY Href"`
	Text string `json:"MY Text"`
}

func GetLinksInfoFromHTMLString(htmlStr string, tagName string) []info {

	infoSlice := make([]info, 0, 1)

	reader := strings.NewReader(htmlStr)

	doc, err := html.Parse(reader)
	if err != nil {
		// ...
	}
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == tagName {
			var infoObj info
			infoObj.Href = n.Attr[0].Val
			text := &bytes.Buffer{}
			collectText(n, text)
			textStr := text.String()
			textStr = strings.TrimSpace(textStr)
			infoObj.Text = textStr
			infoSlice = append(infoSlice, infoObj)
			// fmt.Printf("text.String() %v\n", text.String())
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)

	return infoSlice
}

func collectText(n *html.Node, buf *bytes.Buffer) {
	if n.Type == html.TextNode {
		buf.WriteString(n.Data)
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		collectText(c, buf)
	}
}
