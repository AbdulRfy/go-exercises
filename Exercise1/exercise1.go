package main

import (
	"fmt"
	"unicode"
)

func main() {
	var str = "AbcDefGhk232I2Ruip"
	fmt.Println("-----------> Words count:", camelCaseCount(str))
	fmt.Println("-----------> Words :", wordsInString(str))
}

func camelCaseCount(str string) int {
	var firstChar = rune(str[0])
	var isFirstCaps = unicode.IsUpper(firstChar)

	var count = 1
	if isFirstCaps {
		count = 0
	}
	for _, maRune := range str {
		if unicode.IsUpper(maRune) {
			count++
		}
	}
	return count
}

func wordsInString(str string) []string { // I know this wasnt part of the question but wanted to play with slices a little
	var firstChar = rune(str[0])
	var isFirstCaps = unicode.IsUpper(firstChar)

	stringSlice := make([]string, 0)
	// stringSlice2 := make([]string, 0, 2)
	// stringSlice2[0] = "abc" // error panic out of index

	runeSlice := make([]rune, 0, 6)
	for _, maRune := range str {
		if unicode.IsUpper(maRune) {
			if isFirstCaps { // just to ignore the first letter of string if it is capsed for ma code logic
				isFirstCaps = false
			} else {
				stringSlice = append(stringSlice, string(runeSlice))
				runeSlice = runeSlice[:0]
			}
			runeSlice = append(runeSlice, maRune)
			fmt.Printf("If    runeSlice : %c\n", runeSlice)
			fmt.Printf("stringSlice : %v \n", stringSlice)
		} else {
			runeSlice = append(runeSlice, maRune)
			fmt.Printf("Else  runeSlice : %c\n", runeSlice)
		}
	}
	stringSlice = append(stringSlice, string(runeSlice))
	return stringSlice
}
