package main

import (
	"encoding/json"
	"fmt"

	"myfilereader.com/myfilereader"
	"myhtmlminiparser.com/myhtmlminiparser"
)

type info struct {
	Href string `json:"MY Href"`
	Text string `json:"MY Text"`
}

func main() {
	println("--------- Question:")
	htmlStr := myfilereader.MyReadFile("ex2.html")
	infoObjSlice := myhtmlminiparser.GetLinksInfoFromHTMLString(htmlStr, "a")
	println("--------- Answer:")
	for _, anInfoObj := range infoObjSlice {
		jsn, err := json.MarshalIndent(anInfoObj, "", "    ")
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("Link", string(jsn))
	}
}
