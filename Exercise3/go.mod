module e3.com/e3

go 1.16

require (
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	myfilereader.com/myfilereader v0.0.0-00010101000000-000000000000
	myhtmlminiparser.com/myhtmlminiparser v0.0.0-00010101000000-000000000000
)

replace myfilereader.com/myfilereader => ../MyFileReader

replace myhtmlminiparser.com/myhtmlminiparser => ../MyHtmlMiniParser
