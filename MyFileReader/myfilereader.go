package myfilereader

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func MyReadFile(fileName string) string {
	f, err := os.OpenFile(fileName, os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	readBytes, err := ioutil.ReadAll(f)
	readStr := strings.TrimSpace(string(readBytes))

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(readStr)

	defer func() {
		if err = f.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	return readStr
}
