package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func main() {
	var fileName = "sample.txt"
	var textToWrite = "ajsydasdjshad"
	writeToFile(textToWrite, fileName)
	readFile(fileName)
}

func writeToFile(stringToWrite string, fileName string) {

	isNewFile := false
	if _, err := os.Stat(fileName); os.IsNotExist(err) {
		isNewFile = true
	}

	f, err := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	if !isNewFile {
		f.WriteString("\n")
	}
	f.WriteString(stringToWrite)
	f.Sync()
	defer func() {
		if err = f.Close(); err != nil {
			log.Fatal(err)
		}
	}() // wrote defer as precaution if code is added below this statement
}

func readFile(fileName string) {
	f, err := os.OpenFile(fileName, os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	readBytes, err := ioutil.ReadAll(f)
	readStr := strings.TrimSpace(string(readBytes))

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(readStr)

	defer func() {
		if err = f.Close(); err != nil {
			log.Fatal(err)
		}
	}()
}
